import { useState } from "react";
import { useHistory } from 'react-router-dom'

const SignUp = () => {
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const history = useHistory();

    const fetchUserByPhone = async (phone) => {
        const res = await fetch(process.env.REACT_APP_URL_API+`/users?phone=${phone}`);
        const data = await res.json();
        return data;
    }

    const fetchUserByEmail = async (remail) => {
        const res = await fetch(process.env.REACT_APP_URL_API+`/users?email=${remail}`);
        const data = await res.json();
        return data;
    }

    const onSubmitTask = async (e) => {
        e.preventDefault();
        if(!name) {
            alert('Please enter the name!');
            return;
        }

        if(!phone) {
            alert('Please enter the phone!');
            return;
        }

        if(!email) {
            alert('Please enter the email!');
            return;
        }

        if(!password) {
            alert('Please enter the password!');
            return;
        }

        const exitedPhone = await fetchUserByPhone(phone);
        if(exitedPhone.length > 0) {
            alert('Your phone is exited!');
            return;
        }

        const exitedEmail = await fetchUserByEmail(email);
        if(exitedEmail.length > 0) {
            alert('Your email is exited!');
            return;
        }

        var resultAddUser = await addNewUser( {name, phone, email, password} );
        setName('');
        setPhone('');
        setEmail('');
        setPassword('');

        if(resultAddUser) {
            //after submit form redirect
            history.push('/sign-in');
        }
        
    }

    const addNewUser = async (datas) => {
        const res = await fetch(process.env.REACT_APP_URL_API+'/users', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(datas)
        })
        const data = await res.json();
        return data;
    }

    return(
        <>
            <div className="container">
                <br />
                <h3>Please register the from below</h3>
                <form onSubmit={onSubmitTask}>
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" className="form-control" id="name" placeholder="Enter name" name="name" value={name} onChange={ (e) => setName(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="phone">Phone:</label>
                        <input type="text" className="form-control" id="phone" placeholder="Enter phone" name="phone" value={phone} onChange={ (e) => setPhone(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email:</label>
                        <input type="email" className="form-control" id="email" placeholder="Enter email" name="email" value={email} onChange={ (e) => setEmail(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" id="pwd" placeholder="Enter password" name="pwd" value={password} onChange={ (e) => setPassword(e.target.value) } />
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </>
        
    )
}

export default SignUp