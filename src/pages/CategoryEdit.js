import { useState, useContext, useEffect } from "react";
import { Context } from "../context/Context";
import { useParams, useHistory } from "react-router-dom";

const CategoryEdit = () => {
    const [name, setName] = useState('');
    const [view_order, setViewOrder] = useState('');
    const [oldCategory, setoldCategory] = useState({});
    const { user } = useContext(Context);
    const history = useHistory();
    let { id } = useParams();

    useEffect(() => {
        const getTasks = async (id) => {
            const getCatFromServer = await getCategoryByID(id);
            
            setName(getCatFromServer[0].name);
            setViewOrder(getCatFromServer[0].vieworder);
            setoldCategory(getCatFromServer[0]);
        }
        getTasks(id);
    }, []);

    const getCategoryByID = async (id) => {
        const res = await fetch(process.env.REACT_APP_URL_API+`/category?id=${id}`);
        const data = await res.json();
        return data;
    }

    const onSubmitTaskUpdate = async (e) => {
        e.preventDefault();
        if(!name) {
            alert('Please enter the name!');
            return;
        }

        if(!view_order) {
            alert('Please enter the view order!');
            return;
        }

        var resultNewCat = await updateCategory( { name: name, vieworder: view_order, userId: user.id } );
        if(resultNewCat)
        {
            setName('');
            setViewOrder('');
            setoldCategory('');
            history.push('/category');
        }
    }

    const updateCategory = async (datas) => {
        // Replace old category to new category data
        const updateCate = { ...oldCategory, name: datas.name, vieworder: datas.vieworder };

        //Request to update on DB.json
        const res = await fetch(process.env.REACT_APP_URL_API+`/category/${id}`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(updateCate)
        })
        const data = await res.json();
        return data;
    }

    
    return(
        <>
            <div className="container">
                <br />
                <h3>From Edit Category</h3>
                <form onSubmit={onSubmitTaskUpdate}>
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" className="form-control" id="name" placeholder="Enter name" name="name" value={name} onChange={ (e) => setName(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="view_order">View Order:</label>
                        <input type="number" className="form-control" id="view_order" placeholder="Enter view order" name="view_order" value={view_order} onChange={ (e) => setViewOrder(e.target.value) } />
                    </div>

                    <button type="submit" className="btn btn-primary btn-login">Submit</button>
                </form>
            </div>
        </>
    )
}

export default CategoryEdit