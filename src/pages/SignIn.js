import { useState, useEffect, useContext } from "react";
import { useHistory } from 'react-router-dom'
import { Context } from "../context/Context";

const SignIn = () => {
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const history = useHistory();

    const fetchUserByPhonePwd = async (phone, pwd) => {
        const res = await fetch(process.env.REACT_APP_URL_API+`/users?phone=${phone}&password=${pwd}`);
        const data = await res.json();
        return data;
    }

    const { dispatch, isFetching, error } = useContext(Context);

    const onSubmitTask = async (e) => {
        e.preventDefault();

        dispatch({type: "LOGIN_START"});

        try {
            var userLogin = await fetchUserByPhonePwd(phone, password);
            
            if(userLogin.length > 0) {
                dispatch({ type: "LOGIN_SUCESS", payload: userLogin[0] });
                history.push('/');
            } else {
                dispatch({type: "LOGIN_FAILURE"});
            }
        } catch (e) {
            dispatch({type: "LOGIN_FAILURE"});
        }
    }

    return(
        <>
            <div className="container">
                <br />
                <h3>Please login the from below</h3>
                <form onSubmit={onSubmitTask}>
                    {error && (
                        <>
                            <div className="alert alert-danger">
                                <strong>LOGIN!</strong> Please check your phone or password.
                            </div>
                        </>
                    )}
                    
                    <div className="form-group">
                        <label htmlFor="phone">Phone:</label>
                        <input type="text" className="form-control" id="phone" placeholder="Enter phone" name="phone" value={phone} onChange={ (e) => setPhone(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" id="pwd" placeholder="Enter password" name="pwd" value={password} onChange={ (e) => setPassword(e.target.value) } />
                    </div>

                    <button type="submit" className="btn btn-primary btn-login" disabled={isFetching}>Submit</button>
                </form>
            </div>
        </>
    )
}

export default SignIn