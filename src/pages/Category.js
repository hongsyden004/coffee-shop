import { useState, useContext, useEffect } from "react";
import { Context } from "../context/Context";
import { NavLink } from 'react-router-dom';
import $ from 'jquery';
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'

const Category = () => {
    const [name, setName] = useState('');
    const [view_order, setViewOrder] = useState('');
    const [categorys, setCategory] = useState([])
    const { user } = useContext(Context);

    useEffect(() => {
        const getTasks = async () => {
            const taskCatServer = await fetchCategory();
            setCategory(taskCatServer);
        }
        
        getTasks();
    }, []);

    const fetchCategory = async () => {
        const res = await fetch(process.env.REACT_APP_URL_API+'/category?userId='+user.id);
        const data = await res.json();
        return data;
    }

    const onSubmitTask = async (e) => {
        e.preventDefault();
        if(!name) {
            alert('Please enter the name!');
            return;
        }

        if(!view_order) {
            alert('Please enter the view order!');
            return;
        }
        var resultNewCat = await addNewCategory( { name: name, vieworder: view_order, userId: user.id } );
        
        if(resultNewCat){
            setName('');
            setViewOrder('');
            setCategory([...categorys, resultNewCat]);
        }
    }

    const addNewCategory = async (datas) => {
        const res = await fetch(process.env.REACT_APP_URL_API+'/category', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(datas)
        })
        const data = await res.json();
        return data;
    }

    const onDeleteCate = (id) => {

        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this data!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then( async (result) => {

            if (result.isConfirmed) 
            {
                const result = await fetch(process.env.REACT_APP_URL_API+`/category/${id}`, { 
                    method: 'DELETE' 
                })

                if(result.status == 200) {
                    Swal.fire(
                        'Deleted!',
                        'Your data has been deleted.',
                        'success'
                    )
                    setCategory(categorys.filter((cate) => cate.id !== id))
                }
            }
        })

    }

    return (
        <>
            <div className="container">
                <br />
                <h3>From Create Category</h3>
                <form onSubmit={onSubmitTask}>
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" className="form-control" id="name" placeholder="Enter name" name="name" value={name} onChange={ (e) => setName(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="view_order">View Order:</label>
                        <input type="number" className="form-control" id="view_order" placeholder="Enter view order" name="view_order" value={view_order} onChange={ (e) => setViewOrder(e.target.value) } />
                    </div>

                    <button type="submit" className="btn btn-primary btn-login">Submit</button>
                </form>
                <hr />

                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>View Order</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>

                        {categorys.length > 0 ? (
                            <>
                                {categorys.map((item) => (

                                    <tr key={item.id}>
                                        <td>{item.name}</td>
                                        <td>{item.vieworder}</td>
                                        <td>
                                            <NavLink to={ "/category/" + item.id } className="btn btn-info">Edit</NavLink>
                                        </td>
                                        <td>
                                            <button type="button" className="btn btn-danger" onClick={() => onDeleteCate(item.id)}>
                                                Delete
                                            </button>

                                        </td>
                                    </tr>
                                ))}
                            </>
                        ) : (
                            <>
                                <tr>
                                    <td colSpan="4">There are no record!</td>
                                </tr>
                            </>
                        ) }
                    </tbody>
                </table>

            </div>
        </>
    )
}

export default Category